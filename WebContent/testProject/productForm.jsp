<%@ page language="java" contentType="text/html; charset=TIS620" pageEncoding="TIS620"%>
<%@ taglib uri="/WEB-INF/tld/struts-nested.tld" prefix="nested"%>

<%@ include file="/include/_headermenu.jsp" %>

<%@page import="com.depthfirst.framework.ums.web.UserSession"%>
<nested:form action="/ts/product" enctype="multipart/form-data">
	<nested:define id="pageCode" property="pageCode" type="String" />
	<nested:define id="editable" property="editable" type="Boolean" />
	<nested:define id="flgWarranty" property="formData.flgWarranty" type="Boolean" />
	<input type="hidden" name="cmd" value="" />
	<input type="hidden" name="data_index" value="" />
	
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><nested:write property="formModeStr" />������˹�Ҩ�</th></h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-md-2"><label class="label-form">Product Code : </label></div>
						<div class="col-md-2"><nested:text styleClass="form-control" property="formData.code" size="10" maxlength="6" lang="property['Product Code',required,,]" disabled="<%= !editable.booleanValue() %>" /></div>
						<div class="col-md-2 col-md-offset-2"><label class="label-form">Product Name : </label></div>
						<div class="col-md-2"><nested:text styleClass="form-control" property="formData.name" lang="property['Product Name',required,,]" disabled="<%= !editable.booleanValue() %>" /></div>
					</div>
					<div class="row">
						<div class="col-md-2"><label class="label-form">Price : </label></div>
						<div class="col-md-4"><nested:text property="formData.priceStr" styleClass="form-control" autoNumber="true" decimalDigit="2" disabled="<%= !editable.booleanValue() %>" lang="property['Price',required]"></nested:text></div>
						<div class="col-md-2"><label class="label-form">Warranty : </label></div>
						<div class="col-md-4" style="display: inline-block;">
							<div class="has-feedback">
						      	<div class="input-group">
							        <span class="input-group-addon"><nested:checkbox styleId="isWarranty" styleClass="checkbox" property="formData.isWarrantyShowStr" value="Y" disabled="<%= !editable.booleanValue() %>"></nested:checkbox></span>
							        <nested:text styleId="warrantyDttm" styleClass="form-control datepicker" property="formData.warrantyDttmStr" placeholder="dd/mm/yyyy" readonly="<%= flgWarranty.booleanValue() %>" disabled="<%= !editable.booleanValue() %>"></nested:text>
							    </div>
						  	</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-2"><label class="label-form">Product Type : </label></div>
						<div class="col-md-4">
							<nested:select styleId="selectProdType" styleClass="form-control select2" property="formData.prodTypeId" disabled="<%= !editable.booleanValue() %>">
								<nested:optionsCollection property="prodTypeList" styleClass="form-control" label="typeName" value="id"/>
							</nested:select>
						</div>
						<div class="col-md-2"><label class="label-form">Unit : </label></div>
						<div class="col-md-4">
							<label class="radio-inline">
								<nested:radio property="formData.unit" styleId="inlineRadio1" value="���" disabled="<%= !editable.booleanValue() %>">���</nested:radio>
							</label>
							<label class="radio-inline">
								<nested:radio property="formData.unit" styleId="inlineRadio2" value="��" disabled="<%= !editable.booleanValue() %>">��</nested:radio>
							</label>
							<label class="radio-inline">
								<nested:radio property="formData.unit" styleId="inlineRadio3" value="���ͧ" disabled="<%= !editable.booleanValue() %>">���ͧ</nested:radio>
							</label>
						</div>
					</div>
					<div class="row">
						<div class="col-md-2"><label class="label-form">Active : </label></div>
						<div class="col-md-4">
							<nested:select styleClass="form-control select2" property="formData.isActive" disabled="<%= !editable.booleanValue() %>">
								<nested:optionsCollection property="isActiveList" />
							</nested:select>
						</div>
					</div>
					<div class="row">
						<div class="form-inline">
							<div class="col-md-2"><label class="label-form">Ṻ��� : </label></div>
							<div class="col-md-4">
								 <div class="inline-box">
								 	<nested:file property="formData.formFile" style="display:inline-block; width:200px" styleClass="form-control" size="20" maxlength="200" lang="property['Ṻ���',,,100]" disabled="<%= !editable.booleanValue() %>" />
									<nested:present property="formData.filePath">
										<a href="<%= ctxPath %>/download/<nested:write property="formData.filePath"/>" class="btn btn-sm btn-primary disabled">��ǹ���Ŵ</a>
									</nested:present>
								 </div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-2"><label class="label-form">Location : </label></div>
						<div class="col-md-2"><nested:text styleId="location-address" property="formData.address" styleClass="form-control" disabled="<%= !editable.booleanValue() %>"/></div>
						<div class="col-md-1"><nested:text styleId="radius" property="formData.radius" styleClass="form-control" disabled="<%= !editable.booleanValue() %>"/></div>
						<div class="col-md-1"><nested:text styleId="latitude" property="formData.latitudeStr" styleClass="form-control" disabled="<%= !editable.booleanValue() %>"/></div>
						<div class="col-md-1"><nested:text styleId="longitude" property="formData.longitudeStr" styleClass="form-control" disabled="<%= !editable.booleanValue() %>"/></div>
					</div>
					<div class="row">
						<div class="col-md-6" style="margin-left:17%">
							<div class="location-picker" style="width: 500px; height: 400px;"></div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-2"><label class="label-form">Description : </label></div>
						<div class="col-md-5"><nested:textarea property="formData.description" styleClass="form-control" rows="3" cols="100" tabindex="7" disabled="<%= !editable.booleanValue() %>"/></div>
					</div>
					<div class="box-footer">
						<div class="row">
							<div class="col-md-12">
								<nested:equal property="editable" value="true"><input class="btn btn-success" type="button" value=" �ѹ�֡ " onclick="doCmd('doSave')" /></nested:equal>
								<input class="btn btn-default" type="button" value=" ��Ѻ� " onclick="doCmd('list')" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</nested:form>
<%@ include file="/include/_footermenu.jsp" %>

<script language="JavaScript">
	$.datepicker.setDefaults({
	    dateFormat: 'dd/mm/yyyy'
	});
	$("#isWarranty").on("click", function(){
		if($('#isWarranty').attr('checked')) {
	    	$("#warrantyDttm").attr("readonly", false);
		} else {
	    	$("#warrantyDttm").attr("readonly", true);
	    	$("#warrantyDttm").attr('readonly', 'readonly');
		}
	});
</script>
<style>
	.inline-box {
		display: inline-block;
		width: 980px;
	}
	a.disabled {
	   pointer-events: none;
	   cursor: default;
	}
</style>