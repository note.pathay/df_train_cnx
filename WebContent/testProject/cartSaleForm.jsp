<%@ page language="java" contentType="text/html; charset=TIS620" pageEncoding="TIS620"%>
<%@ taglib uri="/WEB-INF/tld/struts-nested.tld" prefix="nested"%>

<%@ include file="/include/_headermenu.jsp" %>
	
<nested:form action="/ts/cartSale">
<nested:define id="pageCode" property="pageCode" type="String" />
<nested:define id="editable" property="editable" type="Boolean" />
<input type="hidden" name="cmd" value="" />
<input type="hidden" name="add_page_code" value="" />
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title"><nested:write property="formModeStr" /></h3>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-2">Cart No.</div>
					<div class="col-md-1"><nested:text styleClass="form-control" property="formData.cartNo" size="10" maxlength="10" lang="property['Cart No.',required,,10]" disabled="<%= !editable.booleanValue() %>" /></div>
				</div>
				<%-- <div class="row">
					<div class="col-md-2">Sale Date</div>
					<div class="col-md-3"><nested:text styleClass="form-control datePicker" property="formData.dueDate" size="50" maxlength="200" lang="property['Sale Date',,,]" disabled="<%= !editable.booleanValue() %>" /></div>
				</div> --%>
				<div class="row">
					<div class="col-md-2">Status</div>
					<div class="col-md-2">
						<nested:text styleClass="form-control" property="formData.status" size="10" maxlength="10" lang="property['Status',required,,]" disabled="<%= !editable.booleanValue() %>" />
						<%-- <nested:select styleClass="form-control" property="formData.isAuto" disabled="<%= !editable.booleanValue() %>">
							<nested:optionsCollection property="isActiveList" />
						</nested:select> --%>
					</div>
				</div>
				<hr >
				<div class="row">
					<div class="col-md-12">
						<nested:equal property="editable" value="true"><tr class="trform"><td colspan="4"><a name="a_page" />
							<input class="btn btn-info" type="button" name="addPrivBtn" lang="property[hasPriv]" value="Add" onclick="popupCmd('listModulePage', 800, 600)" />
							<input class="btn btn-danger" type="button" value="Delete" onclick="doCmdA('doDeletePage', 'a_page')" />
						</td></tr></nested:equal>
					</div>
				</div>
				<%-- <div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered table-hover">
								<tr>
									<nested:equal property="editable" value="true"><th width="2%"><input type="checkbox" name="delete_all" onclick="doCheckAll(this.form, 'delete_all', 'delete_ind')" /></th></nested:equal>
									<th>�к��ҹ
										<nested:equal name="<%= pageCode %>" property="createMode" value="false"><br/><nested:select property="formModuleId" onchange="doCmd('changeFormModule')"><option value="">������</option>
											<nested:optionsCollection property="formModuleList" value="id" label="name" /></nested:select></nested:equal>
									</th><th>������˹�Ҩ�</th><th>����˹�Ҩ�</th><th>����˹�Ҩ�</th>
									<% for(int i=0; i<UserSession.RIGHT_LABELS.length; i++) { %>
										<th><%= UserSession.RIGHT_LABELS[i] %>
											<nested:equal name="<%= pageCode %>" property="editable" value="true"><br/>
												<table border="0" width="100%"><tr><td align="center"><input type="checkbox" name="right_all[<%= i %>]" onclick="doCheckRightAll(this.form, <%= i %>)" /></td></tr></table></nested:equal>
										</th><% } %>
								</tr>
								<nested:iterate id="priv" indexId="ind" property="formPrivilegeList"><tr class="tr<%= ind.intValue()%2 %>">
									<nested:equal name="<%= pageCode %>" property="editable" value="true"><td><input type="checkbox" name="delete_ind" value="<%= ind %>" onclick="doCheckId(this.form, 'delete_all', 'delete_ind')" /></td></nested:equal>
									<td><nested:write property="page.module.name" /></td>
									<td><nested:write property="page.pageType.name" /></td>
									<td><nested:write property="page.code" /></td>
									<td><nested:write property="page.name" /></td>
									<nested:iterate id="rightVal" property="rightVals"><td align="center">
										<nested:checkbox property="value" value="Y" disabled="<%= !editable.booleanValue() %>"  onclick="doCheckRightId(this.form, this.name)" />
									</td></nested:iterate>
								</tr></nested:iterate>
							</table>
						</div>
					</div>
				</div> --%>
			</div>
			<div class="box-footer">
				<div class="row">
					<div class="col-md-12">
						<nested:equal property="editable" value="true"><input type="button" class="btn btn-success" value=" �ѹ�֡ " onclick="doCmd('doSave')" /></nested:equal>
						<input type="button" class="btn btn-default" value=" ��Ѻ� " onclick="doCmd('list')" />
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</nested:form>
<%@ include file="/include/_footermenu.jsp" %>