<%@ page language="java" contentType="text/html; charset=TIS620" pageEncoding="TIS620"%>
<%@ taglib uri="/WEB-INF/tld/struts-nested.tld" prefix="nested"%>

<%@ include file="/include/_headermenu.jsp" %>
	
<nested:form action="/ts/productType">
	<nested:define id="editable" property="editable" type="Boolean" />
	<input type="hidden" name="cmd" value="" />
	<input type="hidden" name="data_index" value="" />
	<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title"><nested:write property="formModeStr" />������˹�Ҩ�</th></h3>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-2"><label class="label-form">Product Code : </label></div>
					<div class="col-md-2"><nested:text styleClass="form-control" property="formData.typeCode" size="10" maxlength="6" lang="property['����˹�Ҩ�',required,,]" disabled="<%= !editable.booleanValue() %>" /></div>
					<div class="col-md-2 col-md-offset-3"><label class="label-form">Product Type : </label></div>
					<div class="col-md-2"><nested:text property="formData.typeName" styleClass="form-control" size="50" maxlength="200" lang="property['����˹�Ҩ�',required,,200]" disabled="<%= !editable.booleanValue() %>" /></div>
				</div>
				<div class="row">
					<div class="col-md-2"><label class="label-form">Active : </label></div>
					<div class="col-md-2">
						<nested:select styleClass="form-control" property="formData.isActive" disabled="<%= !editable.booleanValue() %>">
							<nested:optionsCollection property="isActiveList"></nested:optionsCollection>
						</nested:select>
					</div>
					
				</div>
				<div class="row">
					<div class="col-md-2"><label class="label-form">Description : </label></div>
					<div class="col-md-5"><nested:textarea property="formData.description" styleClass="form-control" rows="3" cols="100" tabindex="7" disabled="<%= !editable.booleanValue() %>" /></div>
				</div>
			</div>
			<div class="box-footer">
				<div class="row">
					<div class="col-md-12">
							<nested:equal property="editable" value="true"><input class="btn btn-success" type="button" value=" �ѹ�֡ " onclick="doCmd('doSave')" /></nested:equal>
							<input class="btn btn-default" type="button" value=" ��Ѻ� " onclick="doCmd('list')" />
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</nested:form>
<%@ include file="/include/_footermenu.jsp" %>