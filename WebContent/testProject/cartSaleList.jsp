<%@ page language="java" contentType="text/html; charset=TIS620" pageEncoding="TIS620"%>
<%@ taglib uri="/WEB-INF/tld/struts-nested.tld" prefix="nested"%>

<%@ include file="/include/_headermenu.jsp" %>

<nested:form action="/ts/cartSale">
<nested:define id="pageCode" property="pageCode" type="String" />
<nested:define id="searchCondition" property="searchCondition" type="com.depthfirst.framework.search.SearchCondition" />
	<input type="hidden" name="cmd" value="" />
	<input type="hidden" name="data_index" value="" />
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Search</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Cart No : </label>
								<nested:text styleClass="form-control" property="searchCondition.cartNo" size="20" maxlength="20" />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Date : </label><br>
								<nested:text property="searchCondition.startDate" styleClass="form-control datepicker" style="width: 30%; display: inline-block" autoNumber="true" decimalDigit="2"></nested:text>
								<label> To </label>
								<nested:text property="searchCondition.endDate" styleClass="form-control datepicker" style="width: 30%; display: inline-block" autoNumber="true" decimalDigit="2"></nested:text>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Status : </label>
								<%-- <nested:text styleClass="form-control" property="searchCondition.status" size="20" maxlength="20" /> --%>
								<nested:select styleClass="form-control" property="searchCondition.status">
									<option value="">������</option>
									<nested:optionsCollection property="statusList" label="status" value="id"/>
								</nested:select>
							</div>
						</div>
					</div>
				</div>
				<div class="box-footer">
					<div class="row">
						<div class="col-md-12">
							<input class="btn btn-primary" type="button" value=" ���� " onclick="doGoto(0)" />
							<input class="btn btn-primary" type="button" value=" �ʴ������� " onclick="doCmd('listAll')" /> <input class="btn btn-primary" type="button" value=" ������������� " onclick="doCmd('resetSearch')" />
							<input class="btn btn-primary" type="button" value=" ���͡��� Excel " onclick="doCmd('exportExcel')" />
						</div>
					</div>
				</div>
			</div>
			<div class="box box-primary">
				<div class="box-body table-responsive">
					<nested:notEmpty property="searchResult.data">
						<table class="table table-bordered table-hover" border="0" width="95%" align="center">
							<tr class="info">
								<th width="2%">&nbsp;</th>
								<% String thOrderBy[][] = {{"Cart No", "o.cartNo"}, {"Sale Date", "o.dueDate"}, {"Amount", "o.priceAmount"}, {"Status", "o.status"}};
								%><%@ include file="/include/_thorderby.jsp" %>
								<th width="8%">&nbsp;</th>
							</tr>
							<%-- <nested:define id="searchCondition" property="searchCondition" type="com.depthfirst.framework.search.SearchCondition" /> --%>
							<nested:iterate property="searchResult.data" id="result" indexId="ind">
								<tr>
									<td align="right" nowrap="nowrap">&nbsp;<%= ind.intValue()+searchCondition.getPosition()+1 %>&nbsp;</td>
									<td><nested:write name="result" property="cartNo" /></td>
									<td><nested:write name="result" property="dueDateStr" /></td>
									<td><nested:write name="result" property="priceAmountStr" /></td>
									<td><nested:write name="result" property="status"/></td>
									<td nowrap="nowrap">
										&nbsp;<a class="btn btn-info btn-xs" title="�٢�����" href="javascript:doCmd('goView', <%= ind %>)"><i class="fa fa-search" aria-hidden="true"></i></a>&nbsp;
										<nested:equal name="<%= pageCode %>" property="canUpdate" value="true">&nbsp;<a title="���" class="btn btn-warning btn-xs" href="javascript:doCmd('goUpdate', <%= ind %>)"><i class="fa fa-edit" aria-hidden="true"></i></a>&nbsp;</nested:equal>
										<nested:equal name="<%= pageCode %>" property="canDelete" value="true">&nbsp;<a title="ź������" class="btn btn-danger btn-xs" href="javascript:doCmd('doDelete', <%= ind %>)"><i class="fal fa-trash-alt" aria-hidden="true"></i></a>&nbsp;</nested:equal>
										<nested:present property="filePath">&nbsp;<a href="<%= ctxPath %>/download/<nested:write property="filePath"/>"  style="margin-top:20% "><i class="fa fa-download" aria-hidden="true" title="��ǹ���Ŵ"></i></a>&nbsp;</nested:present>
									</td>
								</tr>
							</nested:iterate>
						</table>
						<br>
					<%@ include file="/include/_cursor.jsp" %>
					</nested:notEmpty>
					<nested:empty property="searchResult.data"><nested:equal property="searchResult.processed" value="true">
						<center class="error">��辺�����ŵ�����͹�</center></nested:equal>
					</nested:empty>
				</div>
				<div class="box-footer" style="text-align: center">
					<nested:equal property="canCreate" value="true">
					<input class="btn btn-success" type="button" value=" ���������� " onclick="doCmd('goCreate')" />
					</nested:equal>
				</div>
			</div>
		</div>
	</div>
</nested:form>
<script>
	$.datepicker.setDefaults({
	    dateFormat: 'dd/mm/yyyy'
	});
</script>
<%@ include file="/include/_footermenu.jsp" %>