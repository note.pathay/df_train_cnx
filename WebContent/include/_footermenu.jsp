</section>
		</div>
		<footer class="main-footer">
			<strong>
				Copyright &copy;2018 Depthfirst Co.,Ltd. 
			</strong> All right reserved.
		</footer>
		<div class="control-sidebar-bg"></div>
	</div>

	
<% }catch(Exception _exception){ logger.error(request.getRequestURI(), _exception);_exception.printStackTrace(); } %></td></tr>
<!--end container-->
<%@ include file="/include/_responseMessage.jsp" %>
<%@ include file="/include/_locationPicker.jsp" %>
<!--end footer-->

<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpzziFh3XpWAxnwo0CrDvdBFr5MX-4zTM&libraries=geometry,places&sensor=false"></script> -->
<script language="JavaScript">
<!--
createErrorPane(getForm());
// -->

$('.datepicker').datepicker({language:'th-th',format:'dd/mm/yyyy'})
$('body').on('click', '.day', function(){
	jQuery('.datepicker-dropdown').remove();
});


$('.select2').select2()
$('[data-toggle="tooltip"]').tooltip()

// var ctx = document.getElementById('myChart').getContext('2d');
// var myDoughnutChart = new Chart(ctx, {
//     type: 'doughnut',
//     data: {
//         labels: ["January", "February", "March", "May"],
//         datasets: [{
//             label: "My First dataset",
//             backgroundColor: [
//                 'red',
//                 'blue',
//                 'green',
//                 'orange',
//             ],
//             data: [2, 10, 5, 7],
//         }]
//     },
//     options: {}
// });
</script>

</body>
</html>
