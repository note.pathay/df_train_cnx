<%@page import="com.depthfirst.framework.util.StringUtil"%>
<%@ taglib uri="/WEB-INF/tld/struts-nested.tld" prefix="_nested"%>
<%
org.apache.log4j.Logger logger = org.apache.log4j.LogManager.getLogger( "com.depthfirst.jsp" );
String ctxPath = request.getContextPath();
long random = (new java.util.Date()).getTime();
com.depthfirst.framework.ums.web.UserSession userSession = 
(com.depthfirst.framework.ums.web.UserSession)session.getAttribute(com.depthfirst.framework.ums.web.UserSession.SESSION_KEY);
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="expires" content="0" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="Content-Type" content="text/html; charset=TIS-620" />
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<title> DF PORTAL [<%= java.net.Inet4Address.getLocalHost().getHostName() %>] </title>
<%-- <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/km_back/css/style-doh.css" media="all" /> --%>
<%-- <link type="text/css" href="<%= request.getContextPath() %>/km_back/css/table.css" rel="stylesheet" /> --%>
<!-- <!-- FOR LEFT MENU  -->
<%-- <link type="text/css" href="<%= request.getContextPath() %>/km_back/css/navmenu.css" rel="stylesheet" /> --%>
<%-- <link type="text/css" href="<%= request.getContextPath() %>/css/css0.css" rel="stylesheet" /> --%>
<%-- <link type="text/css" href="<%= request.getContextPath() %>/css/style.css" rel="stylesheet" /> --%>

<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="stylesheet" href="<%= request.getContextPath() %>/lib/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="<%= request.getContextPath() %>/lib/font-awesome/css/fontawesome-all.min.css">
<link rel="stylesheet" href="<%= request.getContextPath() %>/lib/dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="<%= request.getContextPath() %>/lib/dist/css/skins/_all-skins.min.css">
<link rel="stylesheet" href="<%= request.getContextPath() %>/lib/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<link rel="stylesheet" href="<%= request.getContextPath() %>/lib/bootstrap-daterangepicker/daterangepicker.css">
<link rel="stylesheet" href="<%= request.getContextPath() %>/lib/select2/dist/css/select2.min.css">
<link rel="stylesheet" href="<%= request.getContextPath() %>/lib/jAlert/jAlert.css" rel="stylesheet" />
<link rel="stylesheet" href="<%= request.getContextPath() %>/lib/sweetalert2/sweetalert2.min.css">
<link rel="stylesheet"href="<%= request.getContextPath() %>/css/custom.css">

<script language="JavaScript">
<!--
var ctxPath = "<%= ctxPath %>";
// -->
</script>
<script language="JavaScript" src="<%= request.getContextPath() %>/js/win.js"></script>
<script language="JavaScript" src="<%= request.getContextPath() %>/js/util.js"></script>
<script language="JavaScript" src="<%= request.getContextPath() %>/js/struts.js"></script>
<%-- <script language="JavaScript" src="<%= request.getContextPath() %>/js/popupCalendar.js"></script> --%>
<script language="JavaScript" src="<%= request.getContextPath() %>/js/validateForm.js"></script>
<script language="JavaScript" src="<%= request.getContextPath() %>/js/validateDate.js"></script>
<script language="JavaScript" src="<%= request.getContextPath() %>/js/validateNumber.js"></script>

<!-- <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script> -->
<%-- <script type="text/javascript" src="<%= request.getContextPath() %>/km_back/js/jquery-1.7.2.min.js"></script> --%>
<script language="JavaScript" type="text/javascript" src="<%= request.getContextPath() %>/lib/jquery/dist/jquery.js"></script>
<script language="JavaScript" type="text/javascript" src="<%= request.getContextPath() %>/km_back/js/jquery.easing.1.3.js"></script>
<script language="JavaScript" type="text/javascript" src="<%= request.getContextPath() %>/km_back/js/jquery.slashc.two-tier-menu.min.js"></script>

<!-- new theme -->
<script language="JavaScript" src="<%= request.getContextPath() %>/lib/jquery-ui/jquery-ui.min.js"></script>
<script language="JavaScript" src="<%= request.getContextPath() %>/lib/bootstrap/dist/js/bootstrap.min.js"></script>
<script language="JavaScript" src="<%= request.getContextPath() %>/js/bootstrap-datepicker.js"></script>
<script language="JavaScript" src="<%= request.getContextPath() %>/js/bootstrap-datepicker-thai.js"  charset="UTF-8"></script>
<script language="JavaScript" src="<%= request.getContextPath() %>/js/locales/bootstrap-datepicker.th.js"  charset="UTF-8"></script>
<script language="JavaScript" src="<%= request.getContextPath() %>/lib/jquery-knob/dist/jquery.knob.min.js"></script>
<script language="JavaScript" src="<%= request.getContextPath() %>/lib/dist/js/adminlte.min.js"></script>
<script language="JavaScript" src="<%= ctxPath %>/js/Chart.min.js"></script>
<script language="JavaScript" src="<%= request.getContextPath() %>/lib/jAlert/jAlert.js"></script>
<script language="JavaScript" src="<%= request.getContextPath() %>/lib/select2/dist/js/select2.full.min.js"></script>

<script language="JavaScript" type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpzziFh3XpWAxnwo0CrDvdBFr5MX-4zTM&libraries=geometry,places&sensor=false"></script>
<!-- <script src="http://rawgit.com/Logicify/jquery-locationpicker-plugin/master/dist/locationpicker.jquery.js"></script> -->
<%-- <script src="<%= request.getContextPath() %>/lib/location_picker/js/locationpicker.jquery.min.js"></script> --%>
<script language="JavaScript" src="<%= request.getContextPath() %>/lib/location_picker/js/locationpicker.jquery.js"></script>
<script language="JavaScript" src="<%= request.getContextPath() %>/lib/jAlert/jAlert-functions.js"></script>
<script language="JavaScript" src="<%= request.getContextPath() %>/lib/notifyjs/notify.js"></script>
<script language="JavaScript" src="<%= request.getContextPath() %>/lib/sweetalert2/sweetalert2.all.min.js"></script>

<script language="JavaScript">

function doLogout() {
	<%-- if(confirm("�׹�ѹ����͡�ҡ�к�")) {
		document.location.href = '<%= request.getContextPath() %>/login.do?cmd=doLogout&ts='+(new Date()).getTime();
	} --%>
	<%-- $.jAlert({
		  'title': 'Log out',
		  'content': '�׹�ѹ����͡�ҡ�к�',
		  'theme': 'blue',
		  'size': 'sm',
		  'showAnimation': 'fadeInUp',
		  'hideAnimation': 'fadeOutDown',
		  'confirmBtnText':true,
		  'btns': [ {'text': 'Yes', 'onClick': function(){ document.location.href = '<%= request.getContextPath() %>/login.do?cmd=doLogout&ts='+(new Date()).getTime();}}, {'text': 'No'} ]
	});  --%>
	swal({
		title: '�׹�ѹ����͡�ҡ�к�',
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: '�׹�ѹ',
		cancelButtonText: '¡��ԡ',
		confirmButtonClass: 'btn btn-success',
		cancelButtonClass: 'btn btn-danger',
		buttonsStyling: false
	}).then(function (result) {
		if (result.value) {
			document.location.href = '<%= request.getContextPath() %>/login.do?cmd=doLogout&ts='+(new Date()).getTime();
		}
	});
}
$(document).ready(function(){
	$(".trigger").click(function(){
		$(".panel").toggle();
		$(this).toggleClass("active");
		return false;
	});
<_nested:root name="<%= com.depthfirst.framework.ums.web.UserSession.SESSION_KEY %>"><_nested:present property="currentPage.moduleCode">
/*	$(".panel").toggle("fast");
	$(".trigger").toggleClass("active");*/
</_nested:present></_nested:root>
});
</script>
</head>

<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		<header class="main-header fix-top">
			<!-- Logo -->
			<a href="index2.html" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini">DF</span> <!-- logo for regular state and mobile devices -->
				<span class="logo-lg"><b>DF </b>PORTAL</span>
			</a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="push-menu"
					role="button"> <i class="fas fa-bars"></i><span class="sr-only">Toggle navigation</span>
				</a>

				<div class="navbar-custom-menu user-login">
						<%-- <_nested:root name="<%= com.depthfirst.framework.ums.web.UserSession.SESSION_KEY %>">				
						<span class="dropdown user user-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
								<i class="fa fa-user-circle" aria-hidden="true"></i>
								<span class="hidden-xs">
									�Թ�յ�͹�Ѻ : <_nested:write property="login" /> 
				                  	<a class="change-pass" href="<%= ctxPath %>/password.jsp"> <i class="fa fa-key" aria-hidden="true"></i> ����¹���ʼ�ҹ</a>
									<a class="logout" id="logout" href="javascript:doLogout();"> <i class="fa fa-sign-out" aria-hidden="true"></i> �͡�ҡ�к�</a>
								</span>
							</a>
						</span>
						</_nested:root> --%>
						
						<!-- User Account: style can be found in dropdown.less -->
						<_nested:root name="<%= com.depthfirst.framework.ums.web.UserSession.SESSION_KEY %>">
							<span class="dropdown user user-menu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<span class="hidden-xs">
										<i class="fa fa-user-circle" aria-hidden="true"></i> <_nested:write property="login" />
									</span>
								</a>
								<ul class="dropdown-menu">
									<!-- User image -->
									<li class="user-header">
									<div style="background: url('<%= request.getContextPath() %>/images/user-male.png'); height: 150px; background-size: cover; border-radius: 50%; width: 150px; background-position: center; display: inline-block; margin: 10px;"></div>
										<p style="font-weight: bold; color: white;"><_nested:write property="login" /></p>
									</li>
									<!-- Menu Footer-->
									<li class="user-footer">
										<div class="pull-left">
											<a href="<%= ctxPath %>/password.jsp" class="btn btn-warning btn-flat">����¹���ʼ�ҹ</a>
										</div>
										<div class="pull-right">
											<a href="javascript:doLogout();" class="btn btn-danger btn-flat">�͡�ҡ�к�</a>
										</div>
									</li>
								</ul>
							</span>
						</_nested:root>
				</div>
			</nav>
		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar bd-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<ul class="sidebar-menu" data-widget="tree">
					<li class="header">MAIN MENU</li>
					<_nested:root name="<%= com.depthfirst.framework.ums.web.UserSession.SESSION_KEY %>">
					<_nested:notEmpty property="menuItems">
						<_nested:iterate id="mainItem" indexId="mainInd" property="menuItems">
							<_nested:define name="<%= com.depthfirst.framework.ums.web.UserSession.SESSION_KEY %>" id="currentPage" property="currentPage" type="com.depthfirst.framework.ums.web.PageInfo" />
							<_nested:define property="code" id="moduleCode" type="String" />
							<li class="treeview <%= currentPage!=null&&moduleCode.equals(currentPage.getModuleCode())?" active menu-open":"" %>">
								<a href="#">
									<i class="fa fa-<_nested:write property="code" />" aria-hidden="true"></i>
									<span><_nested:write property="name" /></span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i>
									</span>
								</a>
								
								<ul class="treeview-menu" <%= currentPage!=null&&moduleCode.equals(currentPage.getModuleCode())?" style='overflow: hidden; display: block;'":"" %>>
									<_nested:iterate id="subItem" name="mainItem" indexId="subInd" property="subItemList">
										<_nested:define property="name" id="moduleCode2" type="String" />
					              		<_nested:iterate id="menuItem" name="subItem" indexId="menuInd" property="subItemList">
										</_nested:iterate>
							            <li class="treeview <%= currentPage!=null&&moduleCode2.equals(currentPage.getPageType())?" menu-open":"" %>">
					              			<a href="#">
					              				<_nested:write property="name" />
							                	<span class="pull-right-container">
							                  		<i class="fa fa-angle-left pull-right"></i>
							                	</span>
						              		</a>
											<ul class="treeview-menu" <%= currentPage!=null&&moduleCode2.equals(currentPage.getPageType())?" style='overflow: hidden; display: block;'":"" %>>
						              		<_nested:iterate id="menuItem" name="subItem" indexId="menuInd" property="subItemList">
												<_nested:define property="code" id="moduleCode3" type="String" />
												<li<%= currentPage!=null&&moduleCode3.equals(currentPage.getPageCode())?" class='active'":"" %>><a data-toggle="tooltip" data-placement="right" title="<_nested:write property="name" />" href="<%= request.getContextPath() %><_nested:write property="path" />?random=<%= random %>"> <_nested:write property="name" /></a></li>
											</_nested:iterate>
											</ul>
							            </li>
									</_nested:iterate>
								</ul>
							</li>
						</_nested:iterate>
					</_nested:notEmpty>
					</_nested:root>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header content-hg">
				<_nested:root name="<%= com.depthfirst.framework.ums.web.UserSession.SESSION_KEY %>">
					<h1>
						<_nested:present property="currentPage.moduleName"><_nested:write property="currentPage.moduleName" />&nbsp;>>&nbsp;</_nested:present>
						<_nested:write property="currentPage.pageName" /></span>
					</h1>
					<ol class="breadcrumb">
						<li><_nested:write property="currentPage.pageCode" /></li>
					</ol>
				</_nested:root>
			</section>

			<!-- Main content -->
			<section class="content">
<%try { %>
