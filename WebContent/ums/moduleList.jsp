<%@ page language="java" contentType="text/html; charset=TIS620" pageEncoding="TIS620"%>
<%@ taglib uri="/WEB-INF/tld/struts-nested.tld" prefix="nested"%>

<%@ include file="/include/_headermenu.jsp" %>
<nested:form action="/ums/module">
<nested:define id="pageCode" property="pageCode" type="String" />
<input type="hidden" name="cmd" value="" />
<input type="hidden" name="data_index" value="" />
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
<!-- 			<div class="box-header with-border"><h3 class="box-title">���Ң�����</h3> </div> -->
			<div class="box-body table-responsive">
				<nested:notEmpty property="searchResult.data">
					<table class="table table-bordered table-hover" border="0" width="95%" align="center">
						<tr>
							<th width="2%">#</th>
							<th width="10%">�ӴѺ����</th>
							<th width="15%">�ͤ͹</th>
							<th width="70%">�����к��ҹ</th>
							<th width="8%">&nbsp;</th>
						</tr>
						<nested:define id="searchCondition" property="searchCondition" type="com.depthfirst.framework.search.SearchCondition" />
						<nested:iterate property="searchResult.data" id="result" indexId="ind">
						<tr class="tr<%= ind.intValue()%2 %>">
							<td align="right" nowrap="nowrap">&nbsp;<%= ind.intValue()+searchCondition.getPosition()+1 %>&nbsp;</td>
							<td align="right">&nbsp;<nested:write name="result" property="order" />&nbsp;</td>
							<td><i class="fa fa-<nested:write name="result" property="code" />" aria-hidden="true"></i></td>
							<td><nested:write name="result" property="name" /></td>
							<td align="center" nowrap="nowrap">
								&nbsp;<a class="btn btn-info btn-xs" title="�٢�����" href="javascript:doCmd('goView', <%= ind %>)"><i class="fa fa-search" aria-hidden="true"></i></a>&nbsp;
								<nested:equal name="<%= pageCode %>" property="canUpdate" value="true">&nbsp;<a title="���" class="btn btn-warning btn-xs" href="javascript:doCmd('goUpdate', <%= ind %>)"><i class="fa fa-edit" aria-hidden="true"></i></a>&nbsp;</nested:equal>
								<nested:equal name="<%= pageCode %>" property="canDelete" value="true">&nbsp;<a title="ź������" class="btn btn-danger btn-xs" href="javascript:doCmd('doDelete', <%= ind %>)"><i class="fal fa-trash-alt" aria-hidden="true"></i></a>&nbsp;</nested:equal>
							</td>
						</tr>
					</nested:iterate>
					</table>
					<br>
					<%@ include file="/include/_cursor.jsp" %>
				</nested:notEmpty>
			</div>
			<div class="box-footer">
				<div class="row">
					<div class="col-md-12">
						<nested:equal property="canCreate" value="true">
							<center><input class="btn btn-success" type="button" value=" �����������к��ҹ " onclick="doCmd('goCreate')" /></center>
						</nested:equal>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</nested:form>

<%@ include file="/include/_footermenu.jsp" %>
